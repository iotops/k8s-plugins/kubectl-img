# kubectl-img

Simple plugin to list container's images

## Intallation

Download and place the `kubectl-img` file in a directory that is in your `PATH` env var.

Make sure it has execution permisions.

## Verification

Check installation with the following command:

```
$ which kubectl-img
/home/f/bin/kubectl-img
```

Once verified it can be found in your PATHs.

Check if kubectl can see the plugin.

```
$ kubectl plugin list
The following compatible plugins are available:

/home/f/bin/kubectl-img
```

## Usage

Simple run `kubectl img`

```
$ kubectl img
NAME                                           IMAGES
pod1-a0280ba3f5-t7bmz                          docker.io/iotops/flux-helpertools:v2
pod2-53eeb0baf9-c7t8p                          docker.io/iotops/my-coolimage:v2
pod3-8470a4f5c3-nkgiv                          docker.io/iotops/your-coolimage:v3
```

## ToDo

1. add support for namespaces, currently this assumes you are working on the desired namespace
1. intallation via krew
